/*global define*/
define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote'
], function (
    $,
    ko,
    Component,
    quote
) {
    'use strict';
    return Component.extend({
        carrier_code: 'venipak',

        /**
         * Initialization
         * @returns {exports}
         */
        initialize: function () {
            this._super();
            return this;
        },

        /**
         * @returns {exports}
         */
        initObservable: function () {
            this._super();
            ko.computed(function() {
                if (quote.shippingMethod() != null) {
                    this.visibility(quote.shippingMethod());
                }
            }, this);

            return this;
        },

        /**
         * Set visibility of selected carrier
         *
         * @param method
         */
        visibility: function(method)
        {
            this.isVisible((method.carrier_code == this.carrier_code
                && method.method_code == this.method_code));
        }
    });
});
