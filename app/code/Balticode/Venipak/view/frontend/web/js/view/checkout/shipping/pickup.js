define([
    'ko',
    'Balticode_Venipak/js/view/checkout/shipping/generic',
    'Balticode_Venipak/js/action/pickup-points',
    'Balticode_Venipak/js/model/pickup-points',
    'Magento_Checkout/js/model/quote',
], function (
    ko,
    genericShippingComponent,
    terminalAction,
    pickupPoints,
    quote
) {
    'use strict';
    return genericShippingComponent.extend({
        defaults: {
            template: 'Balticode_Venipak/checkout/shipping/pickup'
        },

        isVisible: ko.observable(),

        method_code: 'pickup',

        terminalsMachine: pickupPoints.terminals,
        terminalListIsReady: pickupPoints.terminalListIsReady,

        initialize: function () {
            this._super();

            this.terminalListIsReady.subscribe(function(value) {
                if (value) {
                    this.showTerminalSelection = ko.pureComputed(function () {
                        return pickupPoints.getTerminalList().length !== 0
                    });
                }
            }, this);
        },

        initObservable: function () {
            this._super();

            quote.shippingMethod.subscribe(function () {
                if (this.isVisible()) {
                    this.reloadTerminals();
                }
            }, this);

            this.showTerminalSelection = ko.computed(function() {
                return pickupPoints.getTerminalList().length !== 0
            }, this);

            return this;
        },

        reloadTerminals: function () {
            terminalAction.getTerminalList().done(function (result) {
                if (typeof result !== 'undefined'
                    && result
                    && Array.isArray(result)
                ) {
                    pickupPoints.init(result);
                }
            }.bind(this));
        }
    });
});
