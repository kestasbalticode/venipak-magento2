define([
    'ko',
    'Balticode_Venipak/js/view/checkout/shipping/generic'
], function (
    ko,
    genericShippingComponent
) {
    'use strict';
    return genericShippingComponent.extend({
        defaults: {
            template: 'Balticode_Venipak/checkout/shipping/carrier'
        },

        isVisible: ko.observable(),

        method_code: 'carrier'
    });
});
