/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
/*global define,alert*/
define(
    [
        'Balticode_Venipak/js/model/resource-url-manager',
        'mage/storage'
    ],
    function (
        resourceUrlManager,
        storage
    ) {
        'use strict';
        return {
            saveShippingInformation: function() {
                var payload = {
                    delivery_details: {
                        delivery_time: jQuery('#venipak-carrier-block-wrapper :input[name="delivery_time"]').val(),
                        door_code: jQuery('#venipak-carrier-block-wrapper :input[name="door_code"]').val(),
                        office_number: jQuery('#venipak-carrier-block-wrapper :input[name="office_number"]').val(),
                        warehouse_number: jQuery('#venipak-carrier-block-wrapper :input[name="warehouse_number"]').val(),
                        need_call: jQuery('#venipak-carrier-block-wrapper :input[name="need_call"]')[0].checked
                    }
                };

                return storage.post(
                    resourceUrlManager.getUrlForSetDeliveryDetails(),
                    JSON.stringify(payload)
                ).done(
//                    alert('done')
                ).fail(
//                    alert('fail')
                );
            }
        }
    }
);
