define([
    'Balticode_Venipak/js/model/resource-url-manager',
    'mage/storage',
    'Magento_Checkout/js/model/error-processor',
    'Magento_Checkout/js/model/shipping-service',
], function (
    urlBuilder,
    storage,
    errorProcessor,
    shippingService
) {
    'use strict';

    return {

        /**
         * Get nearest machine list for specified address
         *
         * @return {*}
         */
        getTerminalList: function () {
            shippingService.isLoading(true);
            var serviceUrl = urlBuilder.getUrlForPickupPointsList();
            return storage.get(
                serviceUrl, false
            ).fail(
                function (response) {
                    errorProcessor.process(response);
                }
            ).always(
                function () {
                    shippingService.isLoading(false);
                }
            )
        }
    };
});
