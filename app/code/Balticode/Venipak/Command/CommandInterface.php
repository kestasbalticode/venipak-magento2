<?php

namespace Balticode\Venipak\Command;

/**
 * Interface CommandInterface
 *
 * @package Balticode\Venipak\Command
 */
interface CommandInterface
{
    /**
     * Executes command basing on business object
     *
     * @param array $commandSubject
     * @return array
     */
    public function execute(array $commandSubject = []);
}
