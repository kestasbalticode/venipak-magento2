<?php

namespace Balticode\Venipak\Command;

use Balticode\Venipak\Http\ClientInterface;
use Balticode\Venipak\Http\TransferFactoryInterface;
use Balticode\Venipak\Http\BuilderInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class CallCarrierCommand
 *
 * @package Balticode\Venipak\Command
 */
class CallCarrierCommand implements CommandInterface
{
    /**
     * @var TransferFactoryInterface
     */
    private $transferFactory;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var BuilderInterface
     */
    private $requestBuilder;

    /**
     * Command constructor.
     *
     * @param BuilderInterface $requestBuilder
     * @param TransferFactoryInterface $transferFactory
     * @param ClientInterface $client
     */
    public function __construct(
        BuilderInterface $requestBuilder,
        TransferFactoryInterface $transferFactory,
        ClientInterface $client
    ) {

        $this->transferFactory = $transferFactory;
        $this->client = $client;
        $this->requestBuilder = $requestBuilder;
    }

    /**
     * @param array $commandSubject
     * @return array
     */
    public function execute(array $commandSubject = [])
    {
        $transferObj = $this->transferFactory->create(
            $this->requestBuilder->build($commandSubject)
        );

        $result = $this->client->placeRequest($transferObj);
        $result = $this->parseResult($result);

        return $result;
    }

    /**
     * @param $response
     * @return array
     */
    protected function parseResult($response)
    {
        $response = $response['answer'];

        $code = [];
        if ($response['_attribute']['type'] == 'ok') {
            if (is_array($response['_value']['text'])) {
                $code = $response['_value']['text'];
            } else {
                $code[] = $response['_value']['text'];
            }
        } elseif ($response['_attribute']['type'] == 'error') {
            $messages = [];
            if (array_key_exists('_value', $response['_value']['error'])) {
                $messages[] = $response['_value']['error']['_attribute']['code']
                    .': '.__($response['_value']['error']['_value']['text']);
            } else {
                foreach ($response['_value']['error'] as $errorItem) {
                    $messages[] = $errorItem['_attribute']['code'].': '.__($errorItem['_value']['text']);
                }
            }

            $errorPhrase = new \Magento\Framework\Phrase(
                implode(" / ", $messages)
            );

            throw new LocalizedException($errorPhrase);
        }

        return $code;
    }
}
