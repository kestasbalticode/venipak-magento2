<?php

namespace Balticode\Venipak\Command;

use Balticode\Venipak\Http\ClientInterface;
use Balticode\Venipak\Http\TransferFactoryInterface;
use Balticode\Venipak\Http\BuilderInterface;
use Magento\Framework\Exception\LocalizedException;

/**
 * Class PrintLabelCommand
 *
 * @package Balticode\Venipak\Command
 */
class PrintLabelCommand implements CommandInterface
{
    /**
     * @var TransferFactoryInterface
     */
    private $transferFactory;

    /**
     * @var ClientInterface
     */
    private $client;

    /**
     * @var BuilderInterface
     */
    private $requestBuilder;

    /**
     * Command constructor.
     *
     * @param BuilderInterface $requestBuilder
     * @param TransferFactoryInterface $transferFactory
     * @param ClientInterface $client
     */
    public function __construct(
        BuilderInterface $requestBuilder,
        TransferFactoryInterface $transferFactory,
        ClientInterface $client
    ) {

        $this->transferFactory = $transferFactory;
        $this->client = $client;
        $this->requestBuilder = $requestBuilder;
    }

    /**
     * @param array $commandSubject
     * @return array
     */
    public function execute(array $commandSubject = [])
    {
        $transferObj = $this->transferFactory->create(
            $this->requestBuilder->build($commandSubject)
        );

        try {
            $result = $this->client->placeRequest($transferObj);
        } catch (\Zend_Http_Client_Exception $exception) {
            $errorPhrase = new \Magento\Framework\Phrase(
                $exception->getMessage()
            );
            throw new LocalizedException($errorPhrase);
        }

        if (strpos(strtok($result, "\n"),'%PDF-') === FALSE){
            $errorPhrase = new \Magento\Framework\Phrase(
                'Sorry, but the response what we receive is not like as the PDF document'
            );
            throw new LocalizedException($errorPhrase);
        }

        return $result;
    }
}
