<?php

namespace Balticode\Venipak\Setup;

use Magento\Framework\Setup\UninstallInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * Class Uninstall
 *
 * @package Balticode\Venipak\Setup
 */
class Uninstall implements UninstallInterface
{
    /**
     * @param SchemaSetupInterface   $setup
     * @param ModuleContextInterface $context
     */
    public function uninstall (SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();

        $connection = $setup->getConnection();
        $connection->dropTable($connection->getTableName('venipak_order_data'));
        $connection->dropTable($connection->getTableName('venipak_courier_data'));
        $connection->dropTable($connection->getTableName('venipak_pickup_points'));

        $setup->endSetup();
    }
}
