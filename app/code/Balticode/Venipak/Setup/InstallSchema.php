<?php

namespace Balticode\Venipak\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 *
 * @package Balticode\Venipak\Setup
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @throws \Zend_Db_Exception
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $installer = $setup;
        $installer->startSetup();

        $venipakOrderData = $installer->getTable('venipak_order_data');
// @todo: collect data from interface
        if (!$installer->tableExists($venipakOrderData)) {
            $table = $installer->getConnection()
                ->newTable($venipakOrderData)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary'  => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'quote_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Quote Entity Id'
                )
                ->addColumn(
                    'label_size',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true, 'default' => ''],
                    'Label Size'
                )
                ->addColumn(
                    'pack_count',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true, 'default' => ''],
                    'Pack Count'
                )
                ->addColumn(
                    'warehouse_id',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true, 'default' => ''],
                    'Warehouse Id'
                )
                ->addColumn(
                    'client_warehouse',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true, 'default' => ''],
                    'Client Warehouse'
                )
                ->addColumn(
                    'return_doc',
                    Table::TYPE_BOOLEAN,
                    null,
                    ['nullable' => false, 'default' => 0],
                    'Return Doc'
                )
                ->addColumn(
                    'manifest_no',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true, 'default' => ''],
                    'Manifest No'
                )
                ->addColumn(
                    'pack_no',
                    Table::TYPE_TEXT,
                    3200,
                    ['nullable' => true, 'default' => ''],
                    'Pack No'
                )
                ->addColumn(
                    'time_stamp',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true, 'default' => ''],
                    'Time Stamp'
                )
                ->addColumn(
                    'office_no',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true, 'default' => ''],
                    'Office No'
                )
                ->addColumn(
                    'door_no',
                    Table::TYPE_TEXT,
                    255,
                    ['nullable' => true, 'default' => ''],
                    'Door No'
                )
                ->addColumn(
                    'delivery_call',
                    Table::TYPE_BOOLEAN,
                    null,
                    ['nullable' => false, 'default' => 0],
                    'Delivery Call'
                )
                ->addColumn(
                    'pickup_point_id',
                    Table::TYPE_INTEGER,
                    null,
                    ['nullable' => true, 'default' => null],
                    'Pickup Point ID'
                )
                ->addColumn(
                    'pickup_point_data',
                    Table::TYPE_TEXT,
                    1024,
                    ['nullable' => true, 'default' => null],
                    'Pickup Point Data'
                )
                ->addColumn(
                    'sent',
                    Table::TYPE_BOOLEAN,
                    null,
                    ['nullable' => false, 'default' => 0],
                    'Sent'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                )
                ->setComment('Venipak Delivery Data')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);
        }

        $venipakCourierData = $installer->getTable('venipak_courier_data');

        if (!$installer->tableExists($venipakCourierData)) {
            $table = $installer->getConnection()
                ->newTable($venipakCourierData)
                ->addColumn(
                    'id',
                    Table::TYPE_INTEGER,
                    null,
                    [
                        'identity' => true,
                        'unsigned' => true,
                        'nullable' => false,
                        'primary'  => true
                    ],
                    'ID'
                )
                ->addColumn(
                    'weight',
                    Table::TYPE_TEXT,
                    255,
                    ['unsigned' => true, 'nullable' => false],
                    'Weight'
                )
                ->addColumn(
                    'volume',
                    Table::TYPE_TEXT,
                    255,
                    ['unsigned' => true, 'nullable' => false],
                    'Volume'
                )
                ->addColumn(
                    'pickup_date',
                    Table::TYPE_DATE,
                    null,
                    ['nullable' => false],
                    'Pickup Date'
                )
                ->addColumn(
                    'pickup_time_from',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Pickup Time From'
                )
                ->addColumn(
                    'pickup_time_to',
                    Table::TYPE_TEXT,
                    null,
                    ['nullable' => false],
                    'Pickup Time To'
                )
                ->addColumn(
                    'warehouse_id',
                    Table::TYPE_TEXT,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Warehouse Id'
                )
                ->addColumn(
                    'warehouse_address',
                    Table::TYPE_TEXT,
                    null,
                    ['unsigned' => true, 'nullable' => false],
                    'Warehouse Address'
                )
                ->addColumn(
                    'spp',
                    Table::TYPE_TEXT,
                    255,
                    ['unsigned' => true, 'nullable' => false],
                    'Spp'
                )
                ->addColumn(
                    'doc_no',
                    Table::TYPE_TEXT,
                    255,
                    ['unsigned' => true, 'nullable' => false],
                    'Doc No'
                )
                ->addColumn(
                    'comment',
                    Table::TYPE_TEXT,
                    255,
                    ['unsigned' => true, 'nullable' => false],
                    'Comment'
                )
                ->addColumn(
                    'code',
                    Table::TYPE_TEXT,
                    255,
                    ['unsigned' => true, 'nullable' => false],
                    'Code'
                )
                ->addColumn(
                    'created_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                    'Created At'
                )
                ->addColumn(
                    'updated_at',
                    Table::TYPE_TIMESTAMP,
                    null,
                    ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                    'Updated At'
                )
                ->setComment('Venipak Delivery Data')
                ->setOption('type', 'InnoDB')
                ->setOption('charset', 'utf8');
            $installer->getConnection()->createTable($table);

            $venipakPickupPoints = $installer->getTable('venipak_pickup_points');

            if (!$installer->tableExists($venipakPickupPoints)) {
                $table = $installer->getConnection()
                    ->newTable($venipakPickupPoints)
                    ->addColumn(
                        'entity_id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'identity' => true,
                            'unsigned' => true,
                            'nullable' => false,
                            'primary'  => true
                        ],
                        'Entity ID'
                    )
                    ->addColumn(
                        'id',
                        Table::TYPE_INTEGER,
                        null,
                        [
                            'unsigned' => true,
                            'nullable' => false
                        ],
                        'ID'
                    )
                    ->addColumn(
                        'name',
                        Table::TYPE_TEXT,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'Name'
                    )
                    ->addColumn(
                        'code',
                        Table::TYPE_TEXT,
                        255,
                        ['unsigned' => true, 'nullable' => false],
                        'Code'
                    )
                    ->addColumn(
                        'address',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable' => false],
                        'address'
                    )
                    ->addColumn(
                        'city',
                        Table::TYPE_TEXT,
                        255,
                        ['nullable' => false],
                        'City'
                    )
                    ->addColumn(
                        'zip',
                        Table::TYPE_TEXT,
                        10,
                        ['nullable' => false],
                        'Zip Code'
                    )
                    ->addColumn(
                        'country',
                        Table::TYPE_TEXT,
                        3,
                        ['unsigned' => true, 'nullable' => false],
                        'Country Code'
                    )
                    ->addColumn(
                        'terminal',
                        Table::TYPE_TEXT,
                        125,
                        ['unsigned' => true, 'nullable' => false],
                        'Terminal'
                    )
                    ->addColumn(
                        'description',
                        Table::TYPE_TEXT,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'Description'
                    )
                    ->addColumn(
                        'working_hours',
                        Table::TYPE_TEXT,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'Working Hours'
                    )
                    ->addColumn(
                        'contact_t',
                        Table::TYPE_TEXT,
                        13,
                        ['unsigned' => true, 'nullable' => false],
                        'Contact Phone'
                    )
                    ->addColumn(
                        'lat',
                        Table::TYPE_TEXT,
                        10,
                        ['unsigned' => true, 'nullable' => false],
                        'Latitude'
                    )
                    ->addColumn(
                        'lng',
                        Table::TYPE_TEXT,
                        10,
                        ['unsigned' => true, 'nullable' => false],
                        'Longitude'
                    )
                    ->addColumn(
                        'pick_up_enabled',
                        Table::TYPE_BOOLEAN,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'Pick Up Enabled'
                    )
                    ->addColumn(
                        'cod_enabled',
                        Table::TYPE_BOOLEAN,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'Cod Enabled'
                    )
                    ->addColumn(
                        'ldg_enabled',
                        Table::TYPE_BOOLEAN,
                        null,
                        ['unsigned' => true, 'nullable' => false],
                        'Ldg Enabled'
                    )
                    ->addColumn(
                        'size_limit',
                        Table::TYPE_INTEGER,
                        10,
                        ['unsigned' => true, 'nullable' => false],
                        'Size Limit'
                    )
                    ->addColumn(
                        'created_at',
                        Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => Table::TIMESTAMP_INIT],
                        'Created At'
                    )->addColumn(
                        'updated_at',
                        Table::TYPE_TIMESTAMP,
                        null,
                        ['nullable' => false, 'default' => Table::TIMESTAMP_INIT_UPDATE],
                        'Updated At'
                    )
                    ->setComment('Venipak Pickup Points')
                    ->setOption('type', 'InnoDB')
                    ->setOption('charset', 'utf8');
                $installer->getConnection()->createTable($table);
            }
        }
        $installer->endSetup();
    }
}
