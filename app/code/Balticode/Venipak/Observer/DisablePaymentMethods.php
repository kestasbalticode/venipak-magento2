<?php

namespace Balticode\Venipak\Observer;

use Balticode\Venipak\Helper\Order as OrderHelper;
use Balticode\Venipak\Model\Carrier;
use Magento\Framework\Event\Observer;

/**
 * Class DisablePaymentMethods
 *
 * @package Balticode\Venipak\Observer
 */
class DisablePaymentMethods implements \Magento\Framework\Event\ObserverInterface
{
    /**
     * @var OrderHelper
     */
    protected $orderHelper;

    /**
     * @var Carrier
     */
    protected $carrier;

    /**
     * DisablePaymentMethods constructor.
     *
     * @param OrderHelper $orderHelper
     * @param Carrier     $carrier
     */
    public function __construct (
        OrderHelper $orderHelper,
        Carrier $carrier
    ) {
        $this->orderHelper = $orderHelper;
        $this->carrier = $carrier;
    }

    /**
     * Disable Payment method if not available
     *
     * @param Observer $observer
     */
    public function execute (Observer $observer)
    {
        $quote = $observer->getEvent()->getQuote();
        if ($this->orderHelper->isMyShippingMethod($quote)) {
            $carrier = $this->orderHelper->getCarrierMethod($quote);
            $payment = $observer->getEvent()->getMethodInstance();
            if (!$carrier->isPaymentAvailable($payment)) {
                $result = $observer->getEvent()->getResult();
                $result->setData('is_available', false);
            }
        }
    }
}
