<?php

namespace Balticode\Venipak\Http\Client;

use Balticode\Venipak\Http\ClientInterface;
use Balticode\Venipak\Http\TransferInterface;
use Balticode\Venipak\Http\ConverterInterface;
use Magento\Framework\HTTP\ZendClient;
use Magento\Framework\HTTP\ZendClientFactory;

/**
 * Class Zend
 *
 * @package Balticode\Venipak\Http\Client
 */
class Zend implements ClientInterface
{
    /**
     * @var ZendClientFactory
     */
    private $clientFactory;

    /**
     * @var ConverterInterface | null
     */
    private $converter;

    /**
     * @param ZendClientFactory $clientFactory
     * @param ConverterInterface | null $converter
     */
    public function __construct(
        ZendClientFactory $clientFactory,
        ConverterInterface $converter = null
    ) {
        $this->clientFactory = $clientFactory;
        $this->converter = $converter;
    }

    /**
     * @param TransferInterface $transferObject
     * @return array
     */
    public function placeRequest(TransferInterface $transferObject)
    {
        $result = [];

        /** @var ZendClient $client */
        $client = $this->clientFactory->create();

        $client->setConfig($transferObject->getClientConfig());
        $client->setMethod($transferObject->getMethod());

        $this->setClientRequestParameters($client, $transferObject);

        $client->setHeaders($transferObject->getHeaders());
        $client->setUrlEncodeBody($transferObject->shouldEncode());
        $client->setUri($transferObject->getUri());

        $response = $client->request();

        if ($response->isError()) {
            $phrase = new \Magento\Framework\Phrase($response->getMessage());
            throw new \Magento\Framework\Exception\LocalizedException($phrase);
        }

        $result = $this->handleResponse($response->getBody());

        return $result;
    }

    /**
     * @param ZendClient $client
     * @param TransferInterface $transferObject
     */
    private function setClientRequestParameters(ZendClient $client, TransferInterface $transferObject)
    {
        switch ($transferObject->getMethod()) {
            case \Zend_Http_Client::GET:
                $client->setParameterGet($transferObject->getBody());
                break;
            case \Zend_Http_Client::POST:
                if (is_string($transferObject->getBody())) {
                    $client->setRawData($transferObject->getBody());
                } elseif (is_array($transferObject->getBody()) || is_object($transferObject->getBody())) {
                    $client->setParameterPost((array)$transferObject->getBody());
                }
                break;
            case \Zend_Http_Client::PUT:
                $client->setRawData($transferObject->getBody());
                break;
            default:
                throw new \LogicException(
                    sprintf(
                        'Unsupported HTTP method %s',
                        $transferObject->getMethod()
                    )
                );
        }
    }

    /**
     * @param $response
     * @return array
     */
    private function handleResponse($response)
    {
        return $this->converter
            ? $this->converter->convert($response)
            : $response;
    }
}
