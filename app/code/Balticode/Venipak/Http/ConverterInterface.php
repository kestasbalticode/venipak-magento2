<?php

namespace Balticode\Venipak\Http;

/**
 * Interface ConverterInterface
 *
 * @package Balticode\Venipak\Http
 */
interface ConverterInterface
{
    /**
     * Converts gateway response to ENV structure
     *
     * @param mixed $response
     * @return array
     */
    public function convert($response);
}
