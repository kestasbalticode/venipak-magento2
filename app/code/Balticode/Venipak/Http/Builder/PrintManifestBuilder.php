<?php

namespace Balticode\Venipak\Http\Builder;

use Balticode\Venipak\Http\BuilderInterface;
use Balticode\Venipak\Model\DeliveryDetails\Provider as DeliveryDetails;
use Magento\Framework\Exception\NoSuchEntityException;
use Balticode\Venipak\Helper\Order as OrderHelper;
use Balticode\Venipak\Command\CommandInterface;

/**
 * Class PrintManifestBuilder
 *
 * @package Balticode\Venipak\Http\Builder
 */
class PrintManifestBuilder implements BuilderInterface
{
    protected $store_config;

    protected $deliveryDetails;

    protected $orderHelper;

    protected $command;

    public function __construct (
        \ArrayAccess $store_config,
        DeliveryDetails $deliveryDetails,
        OrderHelper $orderHelper,
        CommandInterface $command
    ) {
        $this->store_config = $store_config;
        $this->deliveryDetails = $deliveryDetails;
        $this->orderHelper = $orderHelper;
        $this->command = $command;
    }

    public function build (array $buildSubject)
    {
        $manifestNo = [];
        foreach ($buildSubject as $order) {
            if ($this->orderHelper->isMyShippingMethod($order)) {
                try {
                    $manifestNumber = $this->deliveryDetails->getByQuoteId($order->getQuoteId())->getManifestNumber();

                    if (!$manifestNumber) {
                        $manifestNumber = $this->command->execute(['order' => $order]);
                    } else {
                        $manifestNumber = explode(',', $manifestNumber);
                    }

                    $manifestNo = array_merge($manifestNo, $manifestNumber);
                } catch (NoSuchEntityException $exception) {

                }
            }
        };

        $manifestNo = array_filter($manifestNo);

        $result = [
            'user' => $this->store_config->getUsername(),
            'pass' => $this->store_config->getPassword(),
            'code' => reset($manifestNo),
            'sandbox' => $this->store_config->getSandbox(),
        ];

        return $result;
    }
}
