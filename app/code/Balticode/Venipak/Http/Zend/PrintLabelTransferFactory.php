<?php

namespace Balticode\Venipak\Http\Zend;

use Balticode\Venipak\Helper\StoreConfig;
use Balticode\Venipak\Http\Transfer\TransferBuilder;
use Balticode\Venipak\Http\TransferFactoryInterface;

/**
 * Class PrintLabelTransferFactory
 *
 * @package Balticode\Venipak\Http\Zend
 */
class PrintLabelTransferFactory implements TransferFactoryInterface
{
    const URL_PATH = 'print_label.php';

    /**
     * @var StoreConfig
     */
    private $storeConfig;

    /**
     * @var TransferBuilder
     */
    private $transferBuilder;

    public function __construct(
        StoreConfig $storeConfig,
        TransferBuilder $transferBuilder
    ) {
        $this->storeConfig = $storeConfig;
        $this->transferBuilder = $transferBuilder;
    }

    public function create(array $request)
    {
        $this->transferBuilder
            ->setBody($request)
            ->setMethod(\Zend_Http_Client::POST)
            ->setClientConfig(['timeout' => 180])
            ->setUri(
                implode(
                    '/',
                    array_filter([
                        rtrim($this->storeConfig->getEndpointUrl(), '/'),
                        self::URL_TARGET_PREFIX,
                        self::URL_PATH
                    ])
                )
            );

        return $this->transferBuilder->build();
    }
}
