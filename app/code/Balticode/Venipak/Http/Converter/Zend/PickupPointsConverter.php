<?php

namespace Balticode\Venipak\Http\Converter\Zend;

use Balticode\Venipak\Http\ConverterInterface;
use Magento\Framework\Serialize\Serializer\Json;

/**
 * Class PickupPointsConverter
 *
 * @package Balticode\Venipak\Http\Converter\Zend
 */
class PickupPointsConverter implements ConverterInterface
{
    /**
     * @var Json
     */
    private $json;

    /**
     * TokenConverter constructor.
     *
     * @param Json $json
     */
    public function __construct(
        Json $json
    ) {
        $this->json = $json;
    }

    /**
     * Converts gateway response to ENV structure
     *
     * @param mixed $response
     *
     * @return array
     */
    public function convert($response)
    {
        $resultResponse = $this->json->unserialize($response);

        return $resultResponse;
    }
}
