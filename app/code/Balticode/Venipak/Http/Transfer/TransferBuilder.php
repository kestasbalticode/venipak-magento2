<?php

namespace Balticode\Venipak\Http\Transfer;

/**
 * Class TransferBuilder
 *
 * @package Balticode\Venipak\Http\Transfer
 */
class TransferBuilder
{
    /**
     * @var array
     */
    private $clientConfig = [];

    /**
     * @var array
     */
    private $headers = [];

    /**
     * @var string
     */
    private $method;

    /**
     * @var array|string
     */
    private $body = [];

    /**
     * @var string
     */
    private $uri = '';

    /**
     * @var bool
     */
    private $encode = false;

    /**
     * @param array $clientConfig
     * @return $this
     */
    public function setClientConfig(array $clientConfig)
    {
        $this->clientConfig = $clientConfig;

        return $this;
    }

    /**
     * @param array $headers
     * @return $this
     */
    public function setHeaders(array $headers)
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @param array|string $body
     * @return $this
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @param string $method
     * @return $this
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @param string $uri
     * @return $this
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * @param bool $encode
     * @return $this
     */
    public function shouldEncode($encode)
    {
        $this->encode = $encode;

        return $this;
    }

    /**
     * @return \Balticode\Venipak\Http\TransferInterface
     */

    /**
     * @return \Balticode\Venipak\Http\Transfer\Transfer
     */
    public function build()
    {
        return new Transfer(
            $this->clientConfig,
            $this->headers,
            $this->body,
            $this->method,
            $this->uri,
            $this->encode
        );
    }
}
