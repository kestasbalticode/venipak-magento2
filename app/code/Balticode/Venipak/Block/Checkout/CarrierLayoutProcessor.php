<?php

namespace Balticode\Venipak\Block\Checkout;

use Balticode\Venipak\Framework\DataObject;

/**
 * Class CarrierLayoutProcessor
 *
 * @package Balticode\Venipak\Block\Checkout
 * @method getElements()
 * @method getJsLayout()
 */
class CarrierLayoutProcessor extends DataObject implements \Magento\Checkout\Block\Checkout\LayoutProcessorInterface
{
    /**
     * @var \Magento\Checkout\Block\Checkout\AttributeMerger
     */
    protected $merger;

    /**
     * @var \Magento\Customer\Api\Data\AddressInterface
     */
    protected $defaultShippingAddress = null;

    protected $dataObject;

    /**
     * CarrierLayoutProcessor constructor.
     *
     * @param \Magento\Checkout\Block\Checkout\AttributeMerger $merger
     * @param array                                            $data
     */
    public function __construct(
        \Magento\Checkout\Block\Checkout\AttributeMerger $merger,
        array $data = []
    ) {
        $this->merger = $merger;
        parent::__construct($data);
    }

    /**
     * Process js Layout of block
     *
     * @param array $jsLayout
     * @return array
     */
    public function process($jsLayout)
    {
        $this->setData('js_layout', $jsLayout);
        $elements = [];
        foreach ($this->getElements() as $elementName => $element) {
            $path = 'js_layout/' . $element['path'];
            $fieldSetPointer = $this->getDataByPath($path);
            if ($fieldSetPointer) {
                $component = [];
                foreach ($element['arguments'] as $attribute => $value) {
                    if (is_object($value) && $value instanceof DataObject) {
                        $component[$attribute] = $value->execute();
                    } else {
                        $component[$attribute] = $value;
                    }
                }
                $elements[$elementName] = $component;

                $fieldSetPointer = $this->merger->merge(
                    $elements,
                    'checkoutProvider',
                    'shippingAddress',
                    $fieldSetPointer
                );

                if (($element['remove_no_visible'] ?? 0)
                    && !($fieldSetPointer[$elementName]['visible'] ?? 0)
                ) {
                    $this->unsetByPath($path. '/' . $elementName);
                } else {
                    $this->setByPath($path, $fieldSetPointer);
                }
            }
        }

        return $this->getJsLayout();
    }
}
