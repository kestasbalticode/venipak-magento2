<?php

namespace Balticode\Venipak\Block\Adminhtml\Button;

use Magento\Framework\View\Element\UiComponent\Control\ButtonProviderInterface;

/**
 * Class CallVenipak
 *
 * @package Balticode\Venipak\Block\Adminhtml\Button
 */
class CallVenipak implements ButtonProviderInterface
{
    /**
     * @return array
     * @codeCoverageIgnore
     */
    public function getButtonData()
    {
        $data = [
            'label' => __('Call Venipak Courier'),
            'class' => 'action-secondary',
            'type' => 'slide',
            'on_click' => '',
            'sort_order' => 80,
            'data_attribute' => [
                'mage-init' => [
                    'Magento_Ui/js/form/button-adapter' => [
                        'actions' => [
                            [
                                'targetName' => 'call_venipak_carrier_form.call_venipak_carrier_form_modal',
                                'actionName' => 'toggleModal'
                            ]
                        ]
                    ]
                ],
            ]
        ];

        return $data;
    }
}
