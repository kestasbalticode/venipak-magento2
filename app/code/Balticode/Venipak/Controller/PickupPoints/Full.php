<?php

namespace Balticode\Venipak\Controller\PickupPoints;

use Balticode\Venipak\Command\CommandInterface;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\RawFactory;
use Magento\Framework\Serialize\SerializerInterface;
use Magento\Framework\Controller\Result\JsonFactory;

/**
 * Class Full
 *
 * @package Balticode\Venipak\Controller\PickupPoints
 */
class Full extends Action
{
    /**
     * @var CommandInterface
     */
    private $command;

    /**
     * @var RawFactory
     */
    private $resultRawFactory;

    /**
     * @var JsonFactory
     */
    private $resultJsonFactory;

    public function __construct(
        Context $context,
        CommandInterface $command,
        RawFactory $resultRawFactory,
        JsonFactory $resultJsonFactory,
        SerializerInterface $serializer
    ) {
        $this->command = $command;
        $this->resultRawFactory = $resultRawFactory;
        $this->resultJsonFactory = $resultJsonFactory;
        $this->serializer = $serializer;

        parent::__construct($context);
    }

    /**
     * Execute action
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $response = $this->command->execute();
        /** @var \Magento\Framework\Controller\Result\Json $resultJson */
        $resultJson = $this->resultJsonFactory->create();

        return $resultJson->setData($response);
    }
}
