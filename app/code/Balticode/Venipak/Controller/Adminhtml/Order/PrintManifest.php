<?php

namespace Balticode\Venipak\Controller\Adminhtml\Order;

use Balticode\Venipak\Command\CommandInterface;
use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Framework\App\Response\Http\FileFactory;

/**
 * Class PrintManifest
 *
 * @package Balticode\Venipak\Controller\Adminhtml\Order
 */
class PrintManifest extends Action
{
    /**
     * @var CommandInterface
     */
    protected $command;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var object
     */
    protected $collectionFactory;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    public function __construct (
        Action\Context $context,
        CommandInterface $command,
        CollectionFactory $collectionFactory,
        Filter $filter,
        FileFactory $fileFactory
    ) {
        $this->command = $command;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->fileFactory = $fileFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());

        $content = $this->command->execute($collection->getItems());

        return $this->fileFactory->create('Manifest.pdf', $content);
    }
}
