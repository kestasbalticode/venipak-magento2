<?php

namespace Balticode\Venipak\Controller\Adminhtml\Order;

use Balticode\Venipak\Command\CommandInterface;
use Magento\Backend\App\Action;
use Magento\Ui\Component\MassAction\Filter;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class PrintLabel
 *
 * @package Balticode\Venipak\Controller\Adminhtml\Order
 */
class PrintLabel extends Action
{
    /**
     * @var CommandInterface
     */
    protected $command;

    /**
     * @var Filter
     */
    protected $filter;

    /**
     * @var object
     */
    protected $collectionFactory;

    /**
     * @var FileFactory
     */
    protected $fileFactory;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    public function __construct (
        Action\Context $context,
        CommandInterface $command,
        CollectionFactory $collectionFactory,
        Filter $filter,
        FileFactory $fileFactory,
        RedirectFactory $resultRedirectFactory,
        ManagerInterface $messageManager
    ) {
        $this->command = $command;
        $this->filter = $filter;
        $this->collectionFactory = $collectionFactory;
        $this->fileFactory = $fileFactory;
        $this->messageManager = $messageManager;
        $this->resultRedirectFactory = $resultRedirectFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $collection = $this->filter->getCollection($this->collectionFactory->create());

        try {
            $content = $this->command->execute($collection->getItems());
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            $result['error'] = true;
            $this->messageManager->addErrorMessage($exception->getMessage());
            $resultRedirect = $this->resultRedirectFactory->create();
            $resultRedirect->setPath('sales/order/index');
            return $resultRedirect;
        }

        return $this->fileFactory->create('Label.pdf', $content);
    }
}
