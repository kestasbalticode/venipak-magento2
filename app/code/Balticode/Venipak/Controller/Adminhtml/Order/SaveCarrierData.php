<?php

namespace Balticode\Venipak\Controller\Adminhtml\Order;

use Magento\Backend\App\Action;
use Magento\Sales\Api\OrderManagementInterface;
use Magento\Sales\Api\OrderRepositoryInterface;
use Psr\Log\LoggerInterface;
use Balticode\Venipak\Api\CarrierDataProviderInterface as CarrierDataProvider;
use Balticode\Venipak\Api\Data\CarrierDataInterface as CarrierData;
use Balticode\Venipak\Model\Source\Warehouse;
use Balticode\Venipak\Helper\StoreConfig;
use Balticode\Venipak\Command\CommandInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\Controller\Result\RedirectFactory;
use Magento\Framework\Message\ManagerInterface;

/**
 * Class SaveCarrierData
 *
 * @package Balticode\Venipak\Controller\Adminhtml\Order
 */
class SaveCarrierData extends \Magento\Sales\Controller\Adminhtml\Order
{
    /**
     * @var CommandInterface
     */
    protected $command;

    /**
     * @var \Magento\Framework\Controller\Result\JsonFactory
     */
    protected $resultJsonFactory;

    /**
     * @var \Balticode\Venipak\Api\CarrierDataProviderInterface
     */
    protected $carrierDataProvider;

    /**
     * @var CarrierData
     */
    protected $carrierData;

    /**
     * @var \Magento\Framework\Registry
     */
    protected $coreRegistry;

    /**
     * @var Warehouse
     */
    protected $warehouse;

    /**
     * @var StoreConfig
     */
    protected $storeConfig;

    /**
     * @var Json
     */
    protected $serializer;

    /**
     * @var RedirectFactory
     */
    protected $resultRedirectFactory;

    /**
     * @var ManagerInterface
     */
    protected $messageManager;

    /**
     * @param Action\Context $context
     * @param \Magento\Framework\Registry $coreRegistry
     * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
     * @param \Magento\Framework\Translate\InlineInterface $translateInline
     * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
     * @param \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory
     * @param \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory
     * @param \Magento\Framework\Controller\Result\RawFactory $resultRawFactory
     * @param OrderManagementInterface $orderManagement
     * @param OrderRepositoryInterface $orderRepository
     * @param LoggerInterface $logger
     * @param \Magento\Framework\View\LayoutFactory $layoutFactory
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
     */
    public function __construct(
        Action\Context $context,
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Response\Http\FileFactory $fileFactory,
        \Magento\Framework\Translate\InlineInterface $translateInline,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory,
        \Magento\Framework\Controller\Result\JsonFactory $resultJsonFactory,
        \Magento\Framework\View\Result\LayoutFactory $resultLayoutFactory,
        \Magento\Framework\Controller\Result\RawFactory $resultRawFactory,
        OrderManagementInterface $orderManagement,
        OrderRepositoryInterface $orderRepository,
        LoggerInterface $logger,
        CarrierDataProvider $carrierDataProvider,
        CarrierData $carrierData,
        Warehouse $warehouse,
        StoreConfig $storeConfig,
        Json $serializer,
        CommandInterface $command,
        RedirectFactory $resultRedirectFactory,
        ManagerInterface $messageManager
    ) {
        $this->resultJsonFactory = $resultJsonFactory;
        $this->carrierDataProvider = $carrierDataProvider;
        $this->coreRegistry = $coreRegistry;
        $this->carrierData = $carrierData;
        $this->warehouse = $warehouse;
        $this->storeConfig = $storeConfig;
        $this->serializer = $serializer;
        $this->command = $command;
        $this->resultRedirectFactory = $resultRedirectFactory;
        $this->messageManager = $messageManager;
        parent::__construct(
            $context,
            $coreRegistry,
            $fileFactory,
            $translateInline,
            $resultPageFactory,
            $resultJsonFactory,
            $resultLayoutFactory,
            $resultRawFactory,
            $orderManagement,
            $orderRepository,
            $logger
        );
    }

    /**
     * Generate order history for ajax request
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $result = [];
        try {
            $postParams = $this->getRequest()->getParams();
            if (is_array($postParams['pickup_date'])) {
                $postParams['pickup_date'] = $postParams['pickup_date']['date'];
            }

            $warehouseParams = $this->storeConfig->getWarehouse($postParams['warehouse_id']);

            $content = $this->command->execute(array_merge($postParams, $warehouseParams));

            $postParams['code'] = implode(', ', $content);
            $postParams['pickup_time_from'] = $postParams['pickup_time_from_h'].':'.$postParams['pickup_time_from_m'];
            $postParams['pickup_time_to'] = $postParams['pickup_time_to_h'].':'.$postParams['pickup_time_to_m'];
            $postParams['warehouse_address'] = $this->serializer->serialize($warehouseParams);

            $result = $this->carrierData->addData($postParams)->save();

            $result['error'] = false;

            $phrase = new \Magento\Framework\Phrase(
                'Courier call success ID: %1',
                [
                    $postParams['code']
                ]
            );

            $this->messageManager->addSuccessMessage($phrase);
        } catch (\Magento\Framework\Exception\LocalizedException $exception) {
            $result['error'] = true;
            $this->messageManager->addErrorMessage($exception->getMessage());
        }

        $resultRedirect = $this->resultRedirectFactory->create();
        $resultRedirect->setPath('sales/order/index');
        return $resultRedirect;
    }
}
