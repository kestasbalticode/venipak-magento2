<?php

namespace Balticode\Venipak\Api\Data;

/**
 * Interface DeliveryDetailsInterface
 *
 * @package Balticode\Venipak\Api\Data
 */
interface PickupPointsInterface
{
    const ENTITY_ID = 'entity_id';
    const ID = 'id';
    const NAME = 'name';
    const CODE = 'code';
    const ADDRESS = 'address';
    const CITY = 'city';
    const ZIP = 'zip';
    const COUNTRY = 'country';
    const TERMINAL = 'terminal';
    const DESCRIPTION = 'description';
    const WORKING_HOURS = 'working_hours';
    const CONTACT_NO = 'contact_t';
    const LAT = 'lat';
    const LANG = 'lng';
    const PICKUP_ENABLED = 'pick_up_enabled';
    const COD_ENABLED = 'cod_enabled';
    const LDG_ENABLES = 'ldg_enabled';
    const SIZE_LIMIT = 'size_limit';
    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';

    /**
     * @return mixed
     */
    public function getId();

    /**
     * @param $value
     * @return mixed
     */
    public function setId($value);

    /**
     * @return mixed
     */
    public function getPickupPointId();

    /**
     * @return mixed
     */
    public function setPickupPointId($value);

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @param $value
     * @return mixed
     */
    public function setName($value);

    /**
     * @return mixed
     */
    public function getCode();

    /**
     * @param $value
     * @return mixed
     */
    public function setCode($value);

    /**
     * @return mixed
     */
    public function getAddress();

    /**
     * @param $value
     * @return mixed
     */
    public function setAddress($value);

    /**
     * @return mixed
     */
    public function getCity();

    /**
     * @param $value
     * @return mixed
     */
    public function setCity($value);

    /**
     * @return mixed
     */
    public function getZip();

    /**
     * @param $value
     * @return mixed
     */
    public function setZip($value);

    /**
     * @return mixed
     */
    public function getCountry();

    /**
     * @param $value
     * @return mixed
     */
    public function setCountry($value);

    /**
     * @return mixed
     */
    public function getTerminal();

    /**
     * @param $value
     * @return mixed
     */
    public function setTerminal($value);

    /**
     * @return mixed
     */
    public function getDescription();

    /**
     * @param $value
     * @return mixed
     */
    public function setDescription($value);

    /**
     * @return mixed
     */
    public function getWorkingHours();

    /**
     * @param $value
     * @return mixed
     */
    public function setWorkingHours($value);

    /**
     * @return mixed
     */
    public function getLat();

    /**
     * @param $value
     * @return mixed
     */
    public function setLat($value);

    /**
     * @return mixed
     */
    public function getLng();

    /**
     * @param $value
     * @return mixed
     */
    public function setLng($value);

    /**
     * @return mixed
     */
    public function getPickUpEnabled();

    /**
     * @param $value
     * @return mixed
     */
    public function setPickUpEnabled($value);

    /**
     * @return mixed
     */
    public function getCodEnabled();

    /**
     * @param $value
     * @return mixed
     */
    public function setCodEnabled($value);

    /**
     * @return mixed
     */
    public function getLdgEnabled();

    /**
     * @param $value
     * @return mixed
     */
    public function setLdgEnabled($value);

    /**
     * @return mixed
     */
    public function getSizeLimit();

    /**
     * @param $value
     * @return mixed
     */
    public function setSizeLimit($value);
}
