<?php

namespace Balticode\Venipak\Api\Data;

/**
 * Interface PickupPointsSearchResultsInterface
 *
 * @package Balticode\Venipak\Api\Data
 */
interface PickupPointsSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get attributes list.
     *
     * @return \Balticode\Venipak\Api\Data\PickupPointsInterface[]
     */
    public function getItems();

    /**
     * Set attributes list.
     *
     * @param \Balticode\Venipak\Api\Data\PickupPointsInterface[] $items
     * @return $this
     */
    public function setItems(array $items);
}
