<?php

namespace Balticode\Venipak\Api;

/**
 * Interface CarrierDataProviderInterface
 *
 * @package Balticode\Venipak\Api
 */
interface CarrierDataProviderInterface
{
    /**
     * Save Carrier Details
     *
     * @param \Balticode\Venipak\Api\Data\CarrierDataInterface $carrierData
     * @return \Balticode\Venipak\Api\Data\CarrierDataInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(\Balticode\Venipak\Api\Data\CarrierDataInterface $carrierData);
}
