<?php

namespace Balticode\Venipak\Model\CarrierData;

use Balticode\Venipak\Api\CarrierDataProviderInterface;
use Balticode\Venipak\Api\Data\CarrierDataInterface;
use Balticode\Venipak\Model\ResourceModel\CarrierData as CarrierDataResource;
use Magento\Framework\Exception\CouldNotSaveException;

/**
 * Class Provider
 *
 * @package Balticode\Venipak\Model\CarrierData
 */
class Provider implements CarrierDataProviderInterface
{
    /**
     * @var CarrierDataResource
     */
    protected $resource;

    /**
     * Provider constructor.
     *
     * @param CarrierDataResource $resource
     */
    public function __construct(
        CarrierDataResource $resource
    ) {
        $this->resource = $resource;
    }

    /**
     * Save Carrier Details
     *
     * @param \Balticode\Venipak\Api\Data\CarrierDataInterface $carrierData
     * @return \Balticode\Venipak\Api\Data\CarrierDataInterface
     * @throws \Magento\Framework\Exception\CouldNotSaveException
     */
    public function save(CarrierDataInterface $carrierData)
    {
        try {
            $this->resource->save($carrierData);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the carrier data: %1',
                $exception->getMessage()
            ));
        }
        return $carrierData;
    }
}
