<?php

namespace Balticode\Venipak\Model\PickupPoints;

use Balticode\Venipak\Api\Data\PickupPointsInterface;
use Balticode\Venipak\Api\PickupPointsProviderInterface;
//use Balticode\Venipak\Model\PickupPointsFactory;
use Balticode\Venipak\Model\ResourceModel\PickupPoints as PickupPointsResource;

use Balticode\Venipak\Model\ResourceModel\PickupPoints\CollectionFactory;
use Balticode\Venipak\Api\Data\PickupPointsSearchResultsInterfaceFactory;

use Balticode\Venipak\Api\Data\PickupPointsInterfaceFactory;
//use Balticode\Venipak\Api\Data\DeliveryDetailsInterface;

use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;


class Provider implements PickupPointsProviderInterface
{
    private $pickupPointsFactory;

    /**
     * @var PickupPointsResource
     */
    protected $resource;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * @var PickupPointsSearchResultsInterface
     */
    protected $searchResultsFactory;

    /**
     * @var CollectionFactory
     */
    protected $collectionFactory;

    public function __construct(
        CollectionFactory $collectionFactory,
        CollectionProcessorInterface $collectionProcessor,
        PickupPointsResource $resource,
        PickupPointsInterfaceFactory $pickupPointsFactory,
        PickupPointsSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->resource = $resource;
        $this->collectionFactory = $collectionFactory;
        $this->collectionProcessor = $collectionProcessor;
        $this->pickupPointsFactory = $pickupPointsFactory;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    public function save(PickupPointsInterface $pickupPoints)
    {
        try {
            try {
                $pPid = $pickupPoints->getPickupPointId();
                if ($row = $this->getByPointId($pPid)) {
                    $pickupPoints->setId($row->getId());
                }
            } catch (NoSuchEntityException $e) {

            }

            $this->resource->save($pickupPoints);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the Pickup Point: %1',
                $exception->getMessage()
            ));
        }

        return $pickupPoints;
    }

    public function getByPointId($entityId)
    {
        $pickupPoint = $this->pickupPointsFactory->create();
        $this->resource->load($pickupPoint, $entityId, PickupPointsInterface::ID);
        if (!$pickupPoint->getId()) {
            throw new NoSuchEntityException(__('Pickup Point for ID="%1" does not exist.', $entityId));
        }
        return $pickupPoint;
    }

    /**
     * @param PickupPointsInterface $pickupPoints
     * @throws CouldNotDeleteException
     */
    public function delete(PickupPointsInterface $pickupPoints)
    {
        try {
            if (!$pickupPoints->getId()) {
                throw new NoSuchEntityException(__('External address does not exist.'));
            }
            $this->resource->delete($pickupPoints);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the external address data: %1',
                $exception->getMessage()
            ));
        }
    }

    /**
     * @param  $searchCriteria
     * @return \Balticode\Venipak\Api\Data\PickupPointsSearchResultsInterface $searchResult
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        /** @var \Balticode\Venipak\Model\ResourceModel\PickupPoints\Collection $collection */
        $collection = $this->collectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);

        /** @var \Balticode\Venipak\Api\Data\PickupPointsSearchResultsInterface $searchResult */
        $searchResult = $this->searchResultsFactory->create();
        $searchResult->setSearchCriteria($searchCriteria);
        $searchResult->setItems($collection->getItems());
        $searchResult->setTotalCount($collection->getSize());

        return $searchResult;
    }
}
