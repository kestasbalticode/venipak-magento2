<?php

namespace Balticode\Venipak\Model\Source;

/**
 * Class DigitList
 *
 * @package Balticode\Venipak\Model\Source
 * @method getSkip()
 * @method getFrom()
 * @method getTo()
 * @method getStep()
 */
class DigitList extends Generic
{
    /**
     * Collecting parameters
     *
     * @return array|mixed|null
     */
    public function collectOptionValues()
    {
        $skip = $this->getSkip() ?? [];
        $list = [];
        for ($i = $this->getFrom(), $n = $this->getTo(), $step = $this->getStep() ?? 1; $i <= $n; $i += $step ) {
            if (!in_array($i, $skip)) {
                $list[$i] = $i;
            }
        }

        return $list;
    }
}
