<?php

namespace Balticode\Venipak\Model\Source;

use \Balticode\Venipak\Helper\StoreConfig;

/**
 * Class Warehouse
 *
 * @package Balticode\Venipak\Model\Source
 */
class Warehouse extends Generic
{
    /**
     * Get available warehouse list merge data into line as one title
     * @return array|mixed|null
     */
    public function execute()
    {
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $storeConfig = $objectManager->create(StoreConfig::class);

        $data = [];
        foreach ($storeConfig->getWarehouse() as $id => $row) {
            $data[$id] = implode(' / ', array_filter([$row['name'], $row['address'], $row['city']]));
        }

        return $data;
    }
}
