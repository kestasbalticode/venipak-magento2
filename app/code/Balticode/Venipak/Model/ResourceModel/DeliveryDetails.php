<?php

namespace Balticode\Venipak\Model\ResourceModel;

use Magento\Framework\Model\ResourceModel\Db\AbstractDb;
use Balticode\Venipak\Api\Data\DeliveryDetailsInterface;

/**
 * Class DeliveryDetails
 *
 * @package Balticode\Venipak\Model\ResourceModel
 */
class DeliveryDetails extends AbstractDb
{
    /**
     * Define resource model
     *
     * @return void
     * @codingStandardsIgnoreStart
     */
    protected function _construct()
    {
        $this->_init('venipak_order_data', DeliveryDetailsInterface::ENTITY_ID);
        // @codingStandardsIgnoreEnd
    }
}
