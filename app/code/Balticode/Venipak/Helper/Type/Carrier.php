<?php

namespace Balticode\Venipak\Helper\Type;

/**
 * Class Carrier
 *
 * @package Balticode\Venipak\Helper\Type
 */
class Carrier extends \Balticode\Venipak\Helper\StoreConfig
{
    /**
     * @param \Magento\Quote\Model\Quote\Address\RateRequest $request
     * @return bool|float
     */
    public function getPrice($request)
    {
        $conditionPrice = $this->getConditionPrice();
        $cartData = $this->collectData($request);
        $hasRowForCurrentCountryId = false;
        $minimalWeightInConditions = null;
        $correctCondition = null;
        foreach ($conditionPrice as $conditionRow) {
            if ($conditionRow['max_weight'] != '') {
                $conditionWeight = $this->convertToFloat($conditionRow['max_weight']);
            } else {
                $conditionWeight = null;
            }

            // This is not correct country
            if (strtoupper($cartData['country_id']) != strtoupper($conditionRow['country'])) {
                continue;
            }

            $hasRowForCurrentCountryId = true;

            if ($conditionWeight !== null && $cartData['weight'] > $conditionWeight) {
                continue;
            }

            if ($minimalWeightInConditions == null || (float)$minimalWeightInConditions >= $conditionWeight) {
                $minimalWeightInConditions = $conditionWeight;
                $correctCondition = $conditionRow;
            }
        }

        if ($correctCondition == null && $hasRowForCurrentCountryId) {
            return false;
        }

        if ($correctCondition == null) {
            $price = parent::getPrice();
            $freeFrom = $this->getFreeFrom();
            if (!empty($freeFrom)) {
                if ($this->convertToFloat($freeFrom) <= $cartData['price']) {
                    $price = 0;
                }
            }

            return (float)$price;
        }

        $price = $correctCondition['price'];

        if (!empty($correctCondition['free_from'])) {
            if ($this->convertToFloat($correctCondition['free_from']) <= $cartData['price']) {
                $price = 0;
            }
        }

        return (float)$price;
    }

    /**
     * It be needed of Older magento version to calculate by packages
     *
     * @param $request
     * @return array
     */
    protected function collectData($request)
    {
        return [
            'country_id' => $request->getDestCountryId(),
            'weight' => (float)$request->getPackageWeight(),
            'price' => (float)$request->getPackageValue()
        ];
    }

    /**
     * Change comma to dot and make ir float type
     *
     * @param int|string|null $value
     * @return float
     */
    protected function convertToFloat($value)
    {
        $value = str_replace(',', '.', $value);
        return (float)$value;
    }
}
