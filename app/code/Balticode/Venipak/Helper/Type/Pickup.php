<?php

namespace Balticode\Venipak\Helper\Type;

/**
 * Class Pickup
 *
 * @package Balticode\Venipak\Helper\Type
 */
class Pickup extends \Balticode\Venipak\Helper\StoreConfig
{
    /**
     * @var \Balticode\Venipak\Api\DeliveryDetailsProviderInterface
     */
    protected $deliveryDetailsInterface;

    /**
     * @var \Balticode\Venipak\Model\PickupPoints\Provider
     */
    protected $pickupPointsProvider;

    /**
     * @param $order
     * @return mixed
     */
    public function getMethodDescription($order)
    {
        $shippingDescription = $order->getShippingDescription();

        if ($order instanceof \Magento\Sales\Api\Data\OrderInterface) {
            try {
                $deliveryDetails = $this->deliveryDetailsInterface->getByQuoteId(
                    $order->getQuoteId()
                );
                $pickupPointId = $deliveryDetails->getPickupPointId();
                $pickUpData = $this->pickupPointsProvider->getByPointId($pickupPointId);

                if ($pickUpData) {
                    $data['name'] = $pickUpData->getName();
                    $data['city'] = $pickUpData->getCity();
                    $data['address'] = $pickUpData->getAddress();
                    $data['country'] = $pickUpData->getCountry() . '-' . $pickUpData->getZip();
                    $data['contact_t'] = $pickUpData->getContactT();
                    $data['description'] = $pickUpData->getDescription();

//                    if ($workingHours = $pickUpData->getWeekWorkingHours()) {
//                        $data['empty_line'] = '&nbsp;';
//                        $data['working_hours'] = implode('<br />', $workingHours);
//                    }

                    $data = array_filter($data);
                }

                $shippingDescription .= ': '.implode(' ', $data);
            }  catch (\Magento\Framework\Exception\LocalizedException $exception) {

            }
        }

        return $shippingDescription;
    }
}
