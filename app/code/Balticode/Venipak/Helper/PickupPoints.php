<?php

namespace Balticode\Venipak\Helper;

/**
 * Class PickupPoints
 *
 * @package Balticode\Venipak\Helper
 */
class PickupPoints extends \Balticode\Venipak\Framework\DataObject
{
    /**
     * @param $list
     * @return array
     */
    public function groupByCity($list)
    {
        $groups = [];
        foreach ($list as $row) {
            $city = $row->getCity('city');
            if (!array_key_exists($city, $groups)) {
                $groups[$city] = [
                    'name' => $city,
                    'terminals' => []
                ];
            }
            $groups[$city]['terminals'][] = $row->getData();
//            sort($groups[$city]['terminals']);
        }
        ksort($groups);

        return $groups;
    }
}
