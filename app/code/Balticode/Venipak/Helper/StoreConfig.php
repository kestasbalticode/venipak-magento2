<?php

namespace Balticode\Venipak\Helper;

use Balticode\Venipak\Api\DeliveryDetailsProviderInterface;
use Balticode\Venipak\Model\PickupPoints\Provider as PickupPointsProvider;
use Magento\Store\Model\ScopeInterface;
use Magento\Framework\App\ScopeInterface as Scope;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Serialize\Serializer\Json;
use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Config\Storage\WriterInterface;
use Magento\Framework\Stdlib\DateTime\DateTime;

/**
 * Class StoreConfig
 *
 * @package Balticode\Venipak\Helper
 * @method getAllowedMethods()
 */
class StoreConfig extends \Balticode\Venipak\Framework\DataObject
{
    /**
     * @var string
     */
    protected $section = '';

    /**
     * @var string
     */
    protected $group = '';

    /**
     * @var string
     */
    protected $field = '';

    /**
     * @var Scope
     */
    protected $scope;

    /**
     * @var Scope
     */
    protected $scopeConfig;

    /**
     * @var string
     */
    protected $type = null;

    /**
     * @var Json
     */
    private $serializer;

    /**
     * @var WriterInterface
     */
    private $configWrite;

    /**
     * @var DateTime
     */
    private $dateTime;

    /**
     * @var DeliveryDetailsProviderInterface
     */
    protected $deliveryDetailsInterface;

    /**
     * @var PickupPointsProvider
     */
    protected $pickupPointsProvider;

    /**
     * StoreConfig constructor.
     *
     * @param ScopeConfigInterface             $scopeConfig
     * @param WriterInterface                  $configWrite
     * @param DateTime                         $dateTime
     * @param DeliveryDetailsProviderInterface $deliveryDetailsInterface
     * @param Scope                            $scope
     * @param string                           $section
     * @param string                           $group
     * @param string|null                      $type
     * @param array                            $data
     * @param Json|null                        $serializer
     */
    public function __construct (
        ScopeConfigInterface $scopeConfig,
        WriterInterface $configWrite,
        DateTime $dateTime,
        DeliveryDetailsProviderInterface $deliveryDetailsInterface,
        PickupPointsProvider $pickupPointsProvider,
        Scope $scope,
        string $section,
        string $group,
        string $type = null,
        array $data = [],
        Json $serializer = null
    ) {
        $this->configWrite = $configWrite;
        $this->scopeConfig = $scopeConfig;
        $this->section = $section;
        $this->group = $group;
        $this->type = $type;
        $this->scope = $scope;
        $this->dateTime = $dateTime;
        $this->deliveryDetailsInterface = $deliveryDetailsInterface;
        $this->pickupPointsProvider = $pickupPointsProvider;

        $this->serializer = $serializer ?: ObjectManager::getInstance()->get(Json::class);

        parent::__construct($data);
        $this->init();
    }

    private function init()
    {
        $data = $this->scopeConfig->getValue(
            implode('/',
                array(
                    $this->section,
                    $this->group
                )
            ),
            ScopeInterface::SCOPE_STORE
            //$this->scope
        );

        parent::addData($data);
    }

    public function __call($method, $args)
    {
        $getter = false;
        switch (substr($method, 0, 3)) {
            case 'get':
                $variableName = substr($method, 3);
                // Without arguments
                if ($variableName == substr(get_called_class(), strrpos(get_called_class(), '\\') + 1)) {
                    if (!($args[0] ?? null)) {
                        return $this;
                    } else {
                        return parent::getData($args[0]);
                    }
                }

                $key = implode('_',
                    array_filter(
                        array(
                            $this->type,
                            $this->_underscore($variableName)
                        )
                    )
                );

                $data = parent::getData($key);
                $getter = true;
                break;
            default:
                $data = parent::__call($method, $args);
                break;
        }

        if (is_string($data)) {
            // If serializer will break some info better save as original
            $orgData = $data;
            try {
                $data = $this->serializer->unserialize($data);
                $this->_data['_temp_value_store_config'] = $data;
            } catch (\InvalidArgumentException $e) {
                $this->_data['_temp_value_store_config'] = $orgData;
            }
        }

        if ($getter) {
            $index = isset($args[0]) ? $args[0] : null;
            $data = parent::getData('_temp_value_store_config', $index);
            $this->unsetData('_temp_value_store_config');
        }

        return $data;
    }

    /**
     * @return string
     */
    public function getEndpointUrl()
    {
        if ($this->getSandbox()) {
            return $this->getApiSandbox();
        }
        return $this->getApi();
    }

    /**
     * Return first array key of warehouse list
     *
     * @return null|string
     */
    public function getDefaultWarehouseId()
    {
        return key($this->getWarehouse());
    }

    /**
     * Generate Package Number
     *
     * @return string
     */
    public function getPackNr()
    {
        $column = 'pack_number';
        $packNumber = $this->getData($column);
        if (!$packNumber) {
            $packNumber = 1;
        }

        $path = implode('/',
            array(
                $this->section,
                $this->group,
                $column
            )
        );

        $this->configWrite->save($path, $packNumber + 1);
        $this->setData($column, $packNumber + 1);

        return sprintf('V%sE%s', $this->getId(), str_pad($packNumber, 7, '0', STR_PAD_LEFT));
    }

    /**
     * Clean Collected data to collect it again just this time new
     *
     * @return mixed
     */
    public function clean()
    {
        return $this->scopeConfig->clean();
    }

    /**
     * Get Full Manifest number by Warehouse ID
     *
     * @param null $warehouseId
     * @return string
     */
    public function getManifestNumber($warehouseId = null)
    {
        if ($warehouseId == null) {
            $warehouseId = $this->getDefaultWarehouseId();
        }

        $warehouse = $this->getWarehouse($warehouseId);

        return sprintf(
            '%s%s%s%s%s',
            $this->getId(),
            $this->dateTime->date('y'),
            $this->dateTime->date('m'),
            $this->dateTime->date('d'),
            str_pad($warehouse['manifest_number'] ?? 1, 3, '0', STR_PAD_LEFT)
        );
    }

    /**
     * Return Available Country Codes
     *
     * @return array
     */
    public function getAvailableCountryCode()
    {
        return explode(',', strtoupper($this->getAvilableCountry()));
    }

    /**
     * Get value by full path
     *
     * @param $path
     * @param $scope
     * @return mixed
     */
    public function getRawValue($path, $scope)
    {
        return $this->scopeConfig->getValue($path, $scope);
    }
}
